package com.example

import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val editor by cssclass()
    }

    init {
        label and heading {
            padding = box(5.px)
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }

        label {
            padding = box(10.px)
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }

        box {spacing=10.px }

        text  and editor{
            minHeight = 700.px
            minWidth = 400.px
            padding = box(10.px)
            fontWeight = FontWeight.BOLD
        }
        button{
            rotate =0.grad
            //spacing = 33.px
            //indent  = 10.px
        }
    }
}