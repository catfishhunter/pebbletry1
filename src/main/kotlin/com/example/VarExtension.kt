package com.example


import com.mitchellbosecke.pebble.extension.AbstractExtension
import com.mitchellbosecke.pebble.extension.NodeVisitor
import com.mitchellbosecke.pebble.lexer.Token
import com.mitchellbosecke.pebble.node.AbstractRenderableNode
import com.mitchellbosecke.pebble.node.AutoEscapeNode
import com.mitchellbosecke.pebble.node.RenderableNode
import com.mitchellbosecke.pebble.parser.Parser
import com.mitchellbosecke.pebble.template.EvaluationContextImpl
import com.mitchellbosecke.pebble.template.PebbleTemplateImpl
import com.mitchellbosecke.pebble.tokenParser.TokenParser
import org.springframework.stereotype.Component
//import ru.itone.dtm.adapter.pebble.extension.util.SqlEscapingStrategy
import java.io.StringWriter
import java.io.Writer


class VarExtension : AbstractExtension() {

    override fun getTokenParsers(): MutableList<TokenParser> {
        return mutableListOf(VarTokenParser())
    }
}

internal class VarTokenParser : TokenParser {
    override fun getTag(): String {
        return VAR_TAG
    }

    override fun parse(token: Token, parser: Parser): RenderableNode {
        val stream = parser.stream
        val lineNumber = token.lineNumber
        stream.next()
        val name = parser.expressionParser.parseNewVariableName()
        stream.expect(Token.Type.EXECUTE_END)
        val body = parser.subparse { data: Token -> data.test(Token.Type.NAME, ENDVAR_TAG) }
        stream.next()
        stream.expect(Token.Type.EXECUTE_END)
        return VarNode(
            lineNumber,
            name,
            AutoEscapeNode(lineNumber, body, true, "html")
        )
    }

    companion object {
        const val VAR_TAG = "var"
        const val ENDVAR_TAG = "endvar"
    }
}

internal class VarNode(lineNUmber: Int, private val name: String, val body: RenderableNode) :
    AbstractRenderableNode(lineNUmber) {

    override fun render(self: PebbleTemplateImpl, writer: Writer, context: EvaluationContextImpl) {
        val varWriter = StringWriter()
        body.render(self, varWriter, context)
        context.scopeChain.put(name, varWriter.toString())
    }

    override fun accept(visitor: NodeVisitor) {
        body.accept(visitor)
    }
}
