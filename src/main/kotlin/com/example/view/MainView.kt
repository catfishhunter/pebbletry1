package com.example.view

import com.example.ROWS_VARIABLE
import com.example.Styles

import tornadofx.*

import javax.swing.plaf.InsetsUIResource

class MainView : View("Try pebble") {
    lateinit private var script: javafx.scene.control.TextArea
    lateinit private var result: javafx.scene.control.TextArea
    lateinit private var name1: javafx.scene.control.TextField
    lateinit private var param1: javafx.scene.control.TextField
    lateinit private var name2: javafx.scene.control.TextField
    lateinit private var param2: javafx.scene.control.TextField

    override val root = vbox {

        label ("Pebble executor"){
            addClass(Styles.heading)
        }
        vbox{
            label("Parameters")
            hbox {
                name1 = textfield("name")
                param1 = textfield("Barsik")
            }
            hbox {
                name2 = textfield("color")
                param2 = textfield("gray")
            }
        }
        vbox {
            label ("Script") {  }

 /*
 {% var query %}
      select * from a1.cat_actual ca
   {% endvar %}

*/
          /*  script=textarea (
                """{% set val=25 %}
{% set users=["aaa","bbb","ccc",2*5+val,"ww"+"qq"] %}
<p> Cats name is {{ name|base64encode }}  цвет {{ color }}.</p>
{% for user in users %}
	<p>{{ user}}</p>
{% endfor %}


""") {*/
                script=textarea (
                    """
    {% set myCn = sdbr ("passenger.cn1") | default ("-1") | tolong %}
    {{ sdbw ("passenger.cn1", myCn+1) }}  
    {% set query="select * from demo_eduejd.colleges " %}
    <x></x>
    {{ sql("col", query) }}
    {% for p in rows.col %}
         <СППВ>
            <col>
               <id>{{ p.id }}</id>
               <name>{{p.full_name}}</name>
            </col>
         </СППВ>
         {% endfor %}


""") {
                /*
                {% set query="select * from a1.cat_actual ca" %}
    <x></x>
    {{ sql("cats", query) }}
    {% for p in rows.cats %}
         <СППВ>
            <cat>
               <id>{{ p.id }}</id>
               <person>
                  <age>{{p.age}}</age>
                  <Имя>{{p.name}}</Имя>
               </person>
            </cat>
         </СППВ>
         {% endfor %}
         */

                addClass(Styles.editor)
            }

        }
        vbox {
            label ("Result") {  }
            result= textarea ("---") {
                addClass(Styles.editor)
            }

        }
        button ("Execute"){action {
            var map=mutableMapOf<String, Any?>(
                ROWS_VARIABLE to HashMap<String, Any?>())


            map.put(name1.text,param1.text)
            map.put(name2.text,param2.text)
            val s = PebbleEvaluator().eval(script.text, map)
            runLater  {
                result.text=s
            }

            //val count: Int = count.text.toInt()
            //SampleSend().generate(count)
        }

        }
        spacing = 10.0
        padding = insets(5)
    }
}
