package com.example.view

import com.example.RegisterFilterExtension
import com.example.SuperPuperExtension
import com.example.VarExtension
import com.mitchellbosecke.pebble.PebbleEngine
import com.mitchellbosecke.pebble.template.PebbleTemplate
import org.springframework.boot.jdbc.DataSourceBuilder
import java.io.StringWriter
import java.io.Writer

import java.util.HashMap
import javax.sql.DataSource


class PebbleEvaluator {

    fun eval(script: String,map: Map<String,Any?>):String
    {
        val dataSource=getDataSource()
        val engine: PebbleEngine = PebbleEngine.Builder().extension(RegisterFilterExtension(),
            SuperPuperExtension(dataSource ,null ),
            VarExtension()
        ).build()

        val compiledTemplate: PebbleTemplate = engine.getLiteralTemplate(script)

        //val context: MutableMap<String, Any> = HashMap()
        //context["name"] = "Mitchell"

        val writer: Writer = StringWriter()
        compiledTemplate.evaluate(writer, map)

        val output: String = writer.toString()
        return output
    }

    fun getDataSource(): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("ru.datamart.prostore.jdbc.Driver")
        dataSourceBuilder.url("jdbc:prostore://10.81.7.90:12720")

        //dataSourceBuilder.driverClassName("org.postgresql.Driver")
        //dataSourceBuilder.url("jdbc:postgresql://localhost:5433/test1")
        //dataSourceBuilder.username("dtm")
        //dataSourceBuilder.password("dtm")
        return dataSourceBuilder.build()
    }
}
