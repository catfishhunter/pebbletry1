/*
 * Project smev3-connector is a part of Data Mart (DM) product
 *   Copyright 2021 and onwards Open Software license Voskhod v 1.0.
 *
 *   This product includes software developed at
 *   IT1 (https://www.it1.ru/)
 *   under strict control of FGAU NII Voskhod (https://www.voskhod.ru/)
 *   for Ministry of Digital Development, Communications and Mass Media of the Russian Federation (https://digital.gov.ru/en/)
 */

package com.example

import com.mitchellbosecke.pebble.extension.AbstractExtension
import com.mitchellbosecke.pebble.extension.Function
import com.mitchellbosecke.pebble.template.EvaluationContext
import com.mitchellbosecke.pebble.template.PebbleTemplate
//import dev.nsud.connector.delta.DELTAS_CONTEXT_VARIABLE
//import dev.nsud.connector.delta.DeltaContext
//import dev.nsud.connector.delta.DeltaService
//import dev.nsud.connector.exception.InvalidConfigurationException
//import dev.nsud.connector.util.SMEV3_DTM_TIMER
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory

import java.io.File
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.beans.factory.annotation.Qualifier
//import org.springframework.stereotype.Component
//import ru.rtlabs.smev.logging.record
import java.sql.ResultSet
import javax.sql.DataSource

const val ROWS_VARIABLE = "rows"

//@Component
class SuperPuperExtension(
    //@Qualifier("pebbleDatasource")
    private val dataSource: DataSource,
    //@Autowired(required = false)
    private val deltaService: Any?// DeltaService?
) : AbstractExtension() {
    override fun getFunctions() = mapOf("log" to LogFunction(), "sql" to SqlFunction(dataSource, deltaService),"sdbr" to SdbrFunction(), "sdbw" to SdbwFunction())
}

class SqlFunction(
    private val ds: DataSource,
    private val deltaService: Any?// DeltaService?
) : Function {

    override fun getArgumentNames() = null

    override fun execute(
        args: Map<String, Any>,
        self: PebbleTemplate,
        context: EvaluationContext,
        lineNumber: Int
    ): Any? {
        runBlocking {
            @Suppress("UNCHECKED_CAST")
            val result = /*mutableMapOf<String, Any?>()*/context.getVariable(ROWS_VARIABLE) as MutableMap<String, Any?>
            //val deltaContext = (context.getVariable(DELTAS_CONTEXT_VARIABLE) as DeltaContext)

            val varName = args["0"] as String
            var sql = args["1"] as String

            /*if (deltaService != null) {
                sql = deltaService.substituteDeltaParams(sql, deltaContext)
            } else {
                if (deltaContext.getEnqueuedDeltasCount() > 0) {
                    throw InvalidConfigurationException("Delta service not found. Please check application properties.")
                }
            }*/

            val startQuery = System.currentTimeMillis()
            ds.connection.use { c ->
                result[varName] = c.prepareStatement(sql).use { st ->
                    var index = 2
                    while (true) {
                        val key = index.toString()
                        if (!args.containsKey(key)) {
                            break
                        }
                        st.setString(index - 1, args[key]?.toString())

                        ++index
                    }

                    st.executeQuery().use { rs ->
                        rs.toRows()
                           /* .also {
                                record(
                                    Duration.ofMillis(System.currentTimeMillis() - startQuery),
                                    SMEV3_DTM_TIMER
                                )
                                deltaService?.preserveDeltaValues(it, deltaContext)
                            }*/
                    }
                }
            }
        }
        return null
    }

    private fun ResultSet.toRows(): List<Map<String, Any?>> {
        //println("--- ")
        val rows = mutableListOf<Map<String, Any?>>()
        while (next()) {
          //  println("next ")

            val row = mutableMapOf<String, Any?>()
            for (i in 1..metaData.columnCount) {
                row[metaData.getColumnName(i)] = getObject(i)
            }
            rows += row
        }
        return rows
    }
}

@Suppress("UNCHECKED_CAST")
class SdbwFunction : Function {

    override fun getArgumentNames() = listOf("key", "value")

    override fun execute(
        rawArgs: Map<String, Any>,
        self: PebbleTemplate,
        context: EvaluationContext,
        lineNumber: Int
    ): String {
        val args = validateAndGet(rawArgs, listOf("key", "value"))
        val key = args["key"] as String
        val value = args["value"].toString()
        println("write to file $key value $value")
        File(key).writeText(value)
        return ""
    }

    private fun validateAndGet(args: Map<String, Any>, requiredFields: List<String>): Map<String, Any> {
        requiredFields.filter { args[it].toString().isEmpty() }
            .forEach { error("Ошибка sdbw") }
        return args
    }
}

class SdbrFunction() : Function {

    override fun getArgumentNames() = listOf("key")

    override fun execute(
        args: Map<String, Any>,
        self: PebbleTemplate,
        context: EvaluationContext,
        lineNumber: Int
    ): Any? {
        val key = validateAndGet(args)

        println("reading file $key ")
        if (!File(key).exists()) return null;
        return File(key).readText()
    }

    private fun validateAndGet(args: Map<String, Any>): String {
        return if (args["key"].toString().isEmpty()) {
            error("error sdbr")
        } else {
            args["key"] as String
        }
    }
}

class LogFunction : Function {

    override fun getArgumentNames(): List<String> =
        listOf("level", "message")

    override fun execute(args: MutableMap<String, Any>, self: PebbleTemplate?, context: EvaluationContext?, lineNumber: Int): Any? {
        val level = args["level"].toString()
        val message = args["message"]?.toString()

       // val logLevel = Level.valueOf(level.uppercase())
        val logger = LoggerFactory.getLogger("LogFunction")
        //logger.info(message)
        println(message)
        /*logger.makeLoggingEventBuilder(logLevel)
            .setMessage(message)
            .log()*/
        return null
    }

   // companion object : KLogging()
}

