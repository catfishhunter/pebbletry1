package com.example

import com.mitchellbosecke.pebble.extension.AbstractExtension
import com.mitchellbosecke.pebble.extension.Filter
import com.mitchellbosecke.pebble.template.EvaluationContext
import com.mitchellbosecke.pebble.template.PebbleTemplate
import java.math.BigDecimal
import java.time.Instant
import java.util.*


class RegisterFilterExtension : AbstractExtension() {
    override fun getFilters(): MutableMap<String, Filter> {
        return mapOf(
            "tolong" to ToLongFilter(),
            "toLong" to ToLongFilter(),
        ).toMutableMap()
    }
}
class ToLongFilter : Filter {

    override fun getArgumentNames(): MutableList<String>? {
        return null
    }

    override fun apply(
        input: Any?,
        args: MutableMap<String, Any>?,
        self: PebbleTemplate?,
        context: EvaluationContext?,
        lineNumber: Int
    ): Long {
        if (input == null) {
            throw IllegalArgumentException("Входные данные не могут быть null")
        }

        return when (input) {
            is Int -> input.toLong()
            is Long -> input
            is String -> {
                if (input.isEmpty()) {
                    error("toLong Filter error")
                } else {
                    BigDecimal(input).toBigInteger().longValueExact()
                }
            }
            is Float -> input.toLong()
            is Double -> input.toLong()
            is java.sql.Date -> input.toLocalDate().toEpochDay()
            is Date -> input.time
            is Instant -> input.toEpochMilli()
            is Boolean -> if (input) 1 else 0
            else -> throw Exception("Unknown type of input var")
        }
    }
}