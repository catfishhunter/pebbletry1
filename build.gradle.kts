import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.32"
    application
    id("com.github.johnrengelman.shadow") version "6.0.0"

}
group = "com.test"
version = "1.0-SNAPSHOT"

val tornadofx_version: String by rootProject
val pebbleVersion: String by rootProject

repositories {
    val nexusPort = System.getProperties()["NEXUS_PORT"]?.toString() ?: "29495"
    val nexusHost = System.getProperties()["NEXUS_HOST"]?.toString() ?: "kuber-smev-dev.gosuslugi.local"
   /* maven {
        url = uri("http://$nexusHost:$nexusPort/repository/maven-public/")
        //url = uri("http://$nexusHost:$nexusPort/repository/maven-public/")
        isAllowInsecureProtocol = true
    }*/
    maven {

        url = uri("http://nexus.gosuslugi.local/content/repositories/releases/")
        isAllowInsecureProtocol = true

        mavenCentral()
    }
}

application {
    mainClassName = "com.example.MainKt"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("no.tornado:tornadofx:$tornadofx_version")
    testImplementation(kotlin("test-junit"))
    implementation("io.pebbletemplates:pebble:$pebbleVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.6.6")
    implementation("org.postgresql:postgresql:42.3.1")
    implementation("ru.datamart.prostore:dtm-jdbc:6.10.0")
    implementation("org.xerial.snappy:snappy-java:1.1.10.5")
    implementation("ch.qos.logback:logback-classic:1.5.6")
    implementation("ch.qos.logback:logback-core:1.5.6")
    implementation("org.slf4j:slf4j-api:2.0.0")


}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}